package ETC;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.Properties;

import org.apache.log4j.Logger;

import AniDB.AniInfo;
import AniDown.AniDownClass.EQuality;

public class DataBaseManager {
	private static Logger logger = Logger.getLogger(DataBaseManager.class);

	private DataBaseManager() {
	}

	private static class Instance {
		public static final DataBaseManager INSTANCE = new DataBaseManager();
	}

	public static DataBaseManager getSingletonObject() {
		return Instance.INSTANCE;
	}

	private Connection conn = null;
	private PreparedStatement stmt = null;
	private ResultSet queryDatas = null;

	private void createStatement() { //TODO : static으로 바꾸어 볼것.
		try {
			Properties props = new Properties();
			File test = new File("./config/db.properties");
			test.getAbsolutePath();
			FileInputStream fis = new FileInputStream("./config/db.properties");
			props.load(new java.io.BufferedInputStream(fis));

			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			conn = DriverManager.getConnection(props.getProperty("mysqlURL"), props.getProperty("mysqlID"),
					props.getProperty("mysqlPW"));
		} catch (Exception e) {
			logger.warn(e);
			e.printStackTrace();
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e1) {
			}
		}
	}

	public LinkedList<AniInfo> SearchMatchingData(LocalDateTime afterDate, LocalDateTime beforeDate) {
		LinkedList<AniInfo> dataList = new LinkedList<AniInfo>();
		try {
			logger.info("DataBaseManager.SearchMatchingData() is Executed... : ");

			createStatement();
			stmt = conn.prepareStatement(
					"SELECT anissia_num, anidb_sourse_url, onnada_sourse_url, broadcasting_date, e_title, k_title from (SELECT * FROM (SELECT anissia.anissia_num, anissia.broadcasting_date, anissia.anissia_site FROM anidb.anissia WHERE ? > anissia.broadcasting_date AND anissia.broadcasting_date >= ? AND anissia.master_num IS NULL) AS anissia_data, (SELECT anidb.anidb_sourse_url, anidb_site.anidb_site, anidb.e_title FROM anidb.anidb, anidb.anidb_site WHERE anidb.anidb_sourse_url = anidb_site.anidb_pk AND ? > anidb.broadcasting_date AND anidb.broadcasting_date >= ? AND anidb.master_num IS NULL) AS anidb_data, (SELECT onnada.onnada_sourse_url, onnada.k_title, onnada_site.onnada_site FROM anidb.onnada, anidb.onnada_site WHERE onnada.onnada_sourse_url = onnada_site.onnada_pk AND ? > onnada.broadcasting_date AND onnada.broadcasting_date >= ? AND onnada.master_num IS NULL) AS onnnada_data) AS p1 WHERE (REPLACE(p1.onnada_site,'/','') = REPLACE(p1.anidb_site,'/','') AND REPLACE(p1.onnada_site,'/','') = REPLACE(p1.anissia_site,'/','') AND REPLACE(p1.anidb_site,'/','') = REPLACE(p1.anissia_site,'/',''))");
			stmt.setDate(1, Date.valueOf(afterDate.toLocalDate()));
			stmt.setDate(2, Date.valueOf(beforeDate.toLocalDate()));
			stmt.setDate(3, Date.valueOf(afterDate.toLocalDate()));
			stmt.setDate(4, Date.valueOf(beforeDate.toLocalDate()));
			stmt.setDate(5, Date.valueOf(afterDate.toLocalDate()));
			stmt.setDate(6, Date.valueOf(beforeDate.toLocalDate()));
			logger.debug(
					"SELECT anissia_num, anidb_sourse_url, onnada_sourse_url, broadcasting_date, e_title, k_title from (SELECT * FROM (SELECT anissia.anissia_num, anissia.broadcasting_date, anissia.anissia_site FROM anidb.anissia WHERE '"
							+ afterDate.toLocalDate()
							+ "' > anissia.broadcasting_date AND anissia.broadcasting_date >= '"
							+ beforeDate.toLocalDate()
							+ "' AND anissia.master_num IS NULL) AS anissia_data, (SELECT anidb.anidb_sourse_url, anidb_site.anidb_site, anidb.e_title FROM anidb.anidb, anidb.anidb_site WHERE anidb.anidb_sourse_url = anidb_site.anidb_pk AND '"
							+ afterDate.toLocalDate() + "' > anidb.broadcasting_date AND anidb.broadcasting_date >= '"
							+ beforeDate.toLocalDate()
							+ "' AND anidb.master_num IS NULL) AS anidb_data, (SELECT onnada.onnada_sourse_url, onnada.k_title, onnada_site.onnada_site FROM anidb.onnada, anidb.onnada_site WHERE onnada.onnada_sourse_url = onnada_site.onnada_pk AND '"
							+ afterDate.toLocalDate() + "' > onnada.broadcasting_date AND onnada.broadcasting_date >= '"
							+ beforeDate.toLocalDate()
							+ "' AND onnada.master_num IS NULL) AS onnnada_data) AS p1 WHERE (REPLACE(p1.onnada_site,'/','') = REPLACE(p1.anidb_site,'/','') AND REPLACE(p1.onnada_site,'/','') = REPLACE(p1.anissia_site,'/','') AND REPLACE(p1.anidb_site,'/','') = REPLACE(p1.anissia_site,'/',''))");
			queryDatas = stmt.executeQuery();

			while (queryDatas.next()) {
				AniInfo aniData = new AniInfo();
				aniData.SetNum(queryDatas.getInt("anissia_num"));
				aniData.SetMysqlMatchinResultSourseUrl(new String[] { queryDatas.getString("anidb_sourse_url"),
						queryDatas.getString("onnada_sourse_url") });
				String englishTitle = queryDatas.getString("e_title");
				aniData.SetEnglisTitle(englishTitle);
				aniData.AddSubTag(englishTitle);
				aniData.SetBroadcastingDate(queryDatas.getTimestamp("broadcasting_date").toLocalDateTime());
				aniData.SetKoreaTitle(queryDatas.getString("k_title"));
				dataList.add(aniData);
			}
			logger.info("DataBaseManager.SearchMatchingData() is Executed END... : ");
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {
			try {
				if (queryDatas != null)
					queryDatas.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return dataList;
	}

	public LinkedList<AniInfo> SearchDownloadDBList(EQuality quality) {
		LinkedList<AniInfo> dataList = new LinkedList<AniInfo>();
		try {
			logger.info("DataBaseManager.SearchDownloadDBList(EQuality quality) is Executed... : ");

			createStatement();
			if (quality == EQuality.TV) {
				stmt = conn.prepareStatement(
						"SELECT integrated_data.master_num, ripper, folder_id, download_date, force_tag, IF( fk_anidb IS NULL, type, type_and_episode ) AS type, IF( fk_onnada IS NULL, e_title, IF(tag IS NULL, e_title, tag )) AS tag FROM anidb.tv_contrl INNER JOIN anidb.tv_ripper_contrl ON tv_contrl.num = tv_ripper_contrl.fk_tv_contrl INNER JOIN anidb.integrated_data ON tv_contrl.fk_integrated = integrated_data.master_num LEFT JOIN anidb.anidb ON tv_contrl.fk_integrated = anidb.master_num LEFT JOIN anidb.onnada ON tv_contrl.fk_integrated = onnada.master_num LEFT JOIN anidb.anissia ON tv_contrl.fk_integrated = anissia.master_num LEFT JOIN anidb.onnada_tag ON onnada.onnada_sourse_url = onnada_tag.onnada_pk WHERE tv_complete = 1 AND DAYOFWEEK(NOW()) = DAYOFWEEK(IF( fk_anidb IS NULL, onnada.broadcasting_date, anidb.broadcasting_date ))");
				logger.debug(
						"SELECT integrated_data.master_num, ripper, folder_id, download_date, force_tag, IF( fk_anidb IS NULL, type, type_and_episode ) AS type, IF( fk_onnada IS NULL, e_title, IF(tag IS NULL, e_title, tag )) AS tag FROM anidb.tv_contrl INNER JOIN anidb.tv_ripper_contrl ON tv_contrl.num = tv_ripper_contrl.fk_tv_contrl INNER JOIN anidb.integrated_data ON tv_contrl.fk_integrated = integrated_data.master_num LEFT JOIN anidb.anidb ON tv_contrl.fk_integrated = anidb.master_num LEFT JOIN anidb.onnada ON tv_contrl.fk_integrated = onnada.master_num LEFT JOIN anidb.anissia ON tv_contrl.fk_integrated = anissia.master_num LEFT JOIN anidb.onnada_tag ON onnada.onnada_sourse_url = onnada_tag.onnada_pk WHERE tv_complete = 1 AND DAYOFWEEK(NOW()) = DAYOFWEEK(IF( fk_anidb IS NULL, onnada.broadcasting_date, anidb.broadcasting_date ))");
			} else {
				stmt = conn.prepareStatement(
						"SELECT integrated_data.master_num, ripper, folder_id, download_date, force_tag, IF( fk_anidb IS NULL, type, type_and_episode ) AS type, IF( fk_onnada IS NULL, e_title, IF(tag IS NULL, e_title, tag )) AS tag FROM anidb.bd_contrl INNER JOIN anidb.bd_ripper_contrl ON bd_contrl.num = bd_ripper_contrl.fk_bd_contrl INNER JOIN anidb.integrated_data ON bd_contrl.fk_integrated = integrated_data.master_num LEFT JOIN anidb.anidb ON bd_contrl.fk_integrated = anidb.master_num LEFT JOIN anidb.onnada ON bd_contrl.fk_integrated = onnada.master_num LEFT JOIN anidb.anissia ON bd_contrl.fk_integrated = anissia.master_num LEFT JOIN anidb.onnada_tag ON onnada.onnada_sourse_url = onnada_tag.onnada_pk WHERE bd_complete = 1 AND DAYOFWEEK(NOW()) = DAYOFWEEK(IF( fk_anidb IS NULL, onnada.broadcasting_date, anidb.broadcasting_date ))");
				logger.debug(
						"SELECT integrated_data.master_num, ripper, folder_id, download_date, force_tag, IF( fk_anidb IS NULL, type, type_and_episode ) AS type, IF( fk_onnada IS NULL, e_title, IF(tag IS NULL, e_title, tag )) AS tag FROM anidb.bd_contrl INNER JOIN anidb.bd_ripper_contrl ON bd_contrl.num = bd_ripper_contrl.fk_bd_contrl INNER JOIN anidb.integrated_data ON bd_contrl.fk_integrated = integrated_data.master_num LEFT JOIN anidb.anidb ON bd_contrl.fk_integrated = anidb.master_num LEFT JOIN anidb.onnada ON bd_contrl.fk_integrated = onnada.master_num LEFT JOIN anidb.anissia ON bd_contrl.fk_integrated = anissia.master_num LEFT JOIN anidb.onnada_tag ON onnada.onnada_sourse_url = onnada_tag.onnada_pk WHERE bd_complete = 1 AND DAYOFWEEK(NOW()) = DAYOFWEEK(IF( fk_anidb IS NULL, onnada.broadcasting_date, anidb.broadcasting_date ))");
			}
			queryDatas = stmt.executeQuery();

			while (queryDatas.next()) {
				AniInfo aniData = new AniInfo();
				aniData.SetMasterNum(queryDatas.getInt("integrated_data.master_num"));
				aniData.SetRipper(queryDatas.getString("ripper"));
				if (quality == EQuality.TV) {
					aniData.SetDownFolderTV(queryDatas.getString("folder_id"));
				} else {
					aniData.SetDownFolderBD(queryDatas.getString("folder_id"));
				}

				String tag = queryDatas.getString("force_tag");
				if (tag.isEmpty()) {
					aniData.SetSubTitleName(queryDatas.getString("tag"));
				} else {
					aniData.SetSubTitleName(tag);
				}

				aniData.SetType(queryDatas.getString("type"));
				logger.debug("result = master_num : " + aniData.GetMasterNum() + ", ripper : " + aniData.GetRipper()
						+ ", down folder : " + queryDatas.getString("folder_id") + ", force tag : " + tag
						+ ", sub title name : " + aniData.GetSubTitleName() + ", type : " + aniData.GetType());

				dataList.add(aniData);
			}
			logger.info("DataBaseManager.SearchDownloadDBList(EQuality quality) is Executed END... : ");
		} catch (Exception e) {
			logger.warn(e);
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
			try {
				if (queryDatas != null)
					queryDatas.close();
			} catch (Exception e) {
			}
		}
		return dataList;
	}

	public String SearchFolderTV(AniInfo aniInfo) {
		String folderID = "";
		try {
			logger.info("DataBaseManager.SearchFolderTV(AniInfo aniInfo) is Executed... : ");

			createStatement();
			stmt = conn.prepareStatement("SELECT folder_id FROM anidb.tv_contrl WHERE fk_integrated = ?");
			stmt.setInt(1, aniInfo.GetMasterNum());
			logger.debug("SELECT folder_id FROM anidb.tv_contrl WHERE fk_integrated = " + aniInfo.GetMasterNum());

			queryDatas = stmt.executeQuery();
			if (queryDatas.next()) {
				folderID = queryDatas.getString("folder_id");
			}
			logger.debug("folderID : " + folderID);

			logger.info("DataBaseManager.SearchFolderTV(AniInfo aniInfo) is Executed END... : ");
		} catch (Exception e) {
			logger.warn(e);
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
			try {
				if (queryDatas != null)
					queryDatas.close();
			} catch (Exception e) {
			}
		}
		return folderID;
	}

	public LinkedList<AniInfo> SearchSubTitle(LocalDateTime afterDate, LocalDateTime beforeDate) {
		LinkedList<AniInfo> dataList = new LinkedList<AniInfo>();
		try {
			logger.info(
					"DataBaseManager.SearchSubTitle(LocalDateTime afterDate, LocalDateTime beforeDate) is Executed... : ");

			createStatement();
			stmt = conn.prepareStatement(
					"SELECT anissia_num FROM anidb.anissia WHERE ? > broadcasting_date AND broadcasting_date >= ?");
			Date after = Date.valueOf(afterDate.toLocalDate());
			Date before = Date.valueOf(beforeDate.toLocalDate());
			stmt.setDate(1, after);
			stmt.setDate(2, before);
			logger.debug("SELECT anissia_num FROM anidb.anissia WHERE '" + after
					 + "' > broadcasting_date AND broadcasting_date >= '"+before+"'");

			queryDatas = stmt.executeQuery();
			while (queryDatas.next()) {
				AniInfo aniData = new AniInfo();
				aniData.SetAniDataNum(queryDatas.getInt("anissia_num"));
				logger.debug("AniDataNum : " + aniData.GetAniDataNum());
				dataList.add(aniData);
			}

			logger.info(
					"DataBaseManager.SearchSubTitle(LocalDateTime afterDate, LocalDateTime beforeDate) is Executed END... : ");
		} catch (Exception e) {
			logger.warn(e);
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
			try {
				if (queryDatas != null)
					queryDatas.close();
			} catch (Exception e) {
			}
		}
		return dataList;
	}

	private LinkedList<String> GetRipper(String path) {
		try {
			LinkedList<String> rippers = new LinkedList<String>();
			FileReader reader = new FileReader(new File(path));
			BufferedReader bufReader = new BufferedReader(reader);
			String line = "";
			while ((line = bufReader.readLine()) != null) {
				rippers.add(line);
			}
			bufReader.close();
			reader.close();
			return rippers;
		} catch (IOException e) {
			System.out.println(e);
			return null;
		}
	}

	public void WriteOnnada(AniInfo aniInfo) {
		try {
			logger.info("DataBaseManager.WriteOnnada(AniInfo aniInfo) is Executed... : ");

			createStatement();
			stmt = conn.prepareStatement(
					"INSERT INTO anidb.onnada(onnada_sourse_url,k_title,o_title,original,director,scenario,character_design,music,company,genre,country,broadcasting_date,rating,image,k_title_rex,episode,type) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE k_title=?,o_title=?,original=?,director=?,scenario=?,character_design=?,music=?,company=?,genre=?,country=?,broadcasting_date=?,rating=?,image=?,k_title_rex=?,episode=?,type=?");
			stmt.setString(1, aniInfo.GetSourseURL());
			stmt.setString(2, aniInfo.GetKoreaTitle());
			stmt.setString(3, aniInfo.GetOriginalTitle());
			stmt.setString(4, aniInfo.GetOriginal());
			stmt.setString(5, aniInfo.GetDirector());
			stmt.setString(6, aniInfo.GetScenario());
			stmt.setString(7, aniInfo.GetCharacterDesign());
			stmt.setString(8, aniInfo.GetMusic());
			stmt.setString(9, aniInfo.GetCompany());
			stmt.setString(10, aniInfo.GetGenre());
			stmt.setString(11, aniInfo.GetCountry());
			Timestamp date = Timestamp.valueOf(aniInfo.GetBroadcastingDate());
			stmt.setTimestamp(12, date);
			stmt.setString(13, aniInfo.GetRating());
			stmt.setString(14, aniInfo.GetImage());
			stmt.setString(15, aniInfo.GetKTitleRex());
			stmt.setString(16, aniInfo.GetCompleteEpisode());
			stmt.setString(17, aniInfo.GetType().toString());

			stmt.setString(18, aniInfo.GetKoreaTitle());
			stmt.setString(19, aniInfo.GetOriginalTitle());
			stmt.setString(20, aniInfo.GetOriginal());
			stmt.setString(21, aniInfo.GetDirector());
			stmt.setString(22, aniInfo.GetScenario());
			stmt.setString(23, aniInfo.GetCharacterDesign());
			stmt.setString(24, aniInfo.GetMusic());
			stmt.setString(25, aniInfo.GetCompany());
			stmt.setString(26, aniInfo.GetGenre());
			stmt.setString(27, aniInfo.GetCountry());
			stmt.setTimestamp(28, date);
			stmt.setString(29, aniInfo.GetRating());
			stmt.setString(30, aniInfo.GetImage());
			stmt.setString(31, aniInfo.GetKTitleRex());
			stmt.setString(32, aniInfo.GetCompleteEpisode());
			stmt.setString(33, aniInfo.GetType().toString());
			logger.debug(
					"INSERT INTO anidb.onnada(onnada_sourse_url,k_title,o_title,original,director,scenario,character_design,music,company,genre,country,broadcasting_date,rating,image,k_title_rex,episode,type) VALUES"
							+ "('" + aniInfo.GetSourseURL() + "', '" + aniInfo.GetKoreaTitle() + "', '"
							+ aniInfo.GetOriginalTitle() + "', " + "'" + aniInfo.GetOriginal() + "', '"
							+ aniInfo.GetDirector() + "', '" + aniInfo.GetScenario() + "', " + "'"
							+ aniInfo.GetCharacterDesign() + "', '" + aniInfo.GetMusic() + "', '" + aniInfo.GetCompany()
							+ "', " + "'" + aniInfo.GetGenre() + "', '" + aniInfo.GetCountry() + "', '" + date + "', '"
							+ aniInfo.GetRating() + "', " + "'" + aniInfo.GetImage() + "', '" + aniInfo.GetKTitleRex()
							+ "', '" + aniInfo.GetCompleteEpisode() + "', '" + aniInfo.GetType().toString()
							+ "') ON DUPLICATE KEY UPDATE " + "k_title='" + aniInfo.GetKoreaTitle() + "',o_title='"
							+ aniInfo.GetOriginalTitle() + "',original='" + aniInfo.GetOriginal() + "'," + "director='"
							+ aniInfo.GetDirector() + "',scenario='" + aniInfo.GetScenario() + "',character_design='"
							+ aniInfo.GetCharacterDesign() + "'," + "music='" + aniInfo.GetMusic() + "',company='"
							+ aniInfo.GetCompany() + "',genre='" + aniInfo.GetGenre() + "'," + "country='"
							+ aniInfo.GetCountry() + "',broadcasting_date='" + date + "',rating='" + aniInfo.GetRating()
							+ "'," + "image='" + aniInfo.GetImage() + "',k_title_rex='" + aniInfo.GetKTitleRex()
							+ "',episode='" + aniInfo.GetCompleteEpisode() + "',type='" + aniInfo.GetType().toString()
							+ "'");

			stmt.executeUpdate();

			logger.info("DataBaseManager.WriteOnnada(AniInfo aniInfo) is Executed END... : ");
		} catch (Exception e) {
			logger.warn(e);
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}

	public void WriteOnnadaTag(AniInfo aniInfo) {
		try {
			logger.info("DataBaseManager.WriteOnnadaTag(AniInfo aniInfo) is Executed... : ");

			createStatement();
			for (String tag : aniInfo.GetSubTag()) {
				stmt = conn.prepareStatement(
						"INSERT INTO anidb.onnada_tag(onnada_pk, tag) VALUES(?,?) ON DUPLICATE KEY UPDATE tag=?");
				stmt.setString(1, aniInfo.GetSourseURL());
				stmt.setString(2, tag);
				stmt.setString(3, tag);
				logger.debug("INSERT INTO anidb.onnada_tag(onnada_pk, tag) VALUES('" + aniInfo.GetSourseURL() + "','"
						+ tag + "') ON DUPLICATE KEY UPDATE tag='" + tag + "'");

				stmt.executeUpdate();
			}

			logger.info("DataBaseManager.WriteOnnadaTag(AniInfo aniInfo) is Executed END... : ");
		} catch (Exception e) {
			logger.warn(e);
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}

	public void WriteOnnadaSite(AniInfo aniInfo) {
		try {
			logger.info("DataBaseManager.WriteOnnadaSite(AniInfo aniInfo) is Executed... : ");

			createStatement();
			for (String site : aniInfo.GetSubSite()) {
				stmt = conn.prepareStatement(
						"INSERT INTO anidb.onnada_site(onnada_pk, onnada_site) VALUES(?,?) ON DUPLICATE KEY UPDATE onnada_site=?");
				stmt.setString(1, aniInfo.GetSourseURL());
				stmt.setString(2, site);
				stmt.setString(3, site);
				logger.debug("INSERT INTO anidb.onnada_site(onnada_pk, onnada_site) VALUES('" + aniInfo.GetSourseURL()
						+ "', '" + site + "') ON DUPLICATE KEY UPDATE onnada_site='" + site + "'");

				stmt.executeUpdate();
			}

			logger.info("DataBaseManager.WriteOnnadaSite(AniInfo aniInfo) is Executed END... : ");
		} catch (Exception e) {
			logger.warn(e);
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}

}