package AniDB;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import AniDown.AniDownClass.EType;
import ETC.WebInfo;

public class Onnada {
	private static Logger logger = Logger.getLogger(Onnada.class);
	private LinkedList<AniInfo> aniList = new LinkedList<AniInfo>();

	public LinkedList<AniInfo> GetAniList() {
		return aniList;
	}

	public void GetData(LocalDateTime date) {
		logger.info("Onnada.GetData(LocalDateTime date) is Executed... : ");
		StringBuilder urlPath = new StringBuilder();

		if (date.isAfter(LocalDateTime.now())) {
			urlPath.append("http://anime.onnada.com/wait.php");
		} else {
			urlPath.append("http://anime.onnada.com/hit.php?nav=hit&c=&q=&at=&date=");
			urlPath.append(date.getYear());
			urlPath.append("&month=");
			DateTimeFormatter monthFormat = DateTimeFormatter.ofPattern("MM");
			urlPath.append(monthFormat.format(date.getMonth()));
			urlPath.append("&rate=&ct=&ct2=");
		}
		logger.debug("url : " + urlPath);

		try {
			Document doc = Jsoup.connect(urlPath.toString()).timeout(WebInfo.timeOut).userAgent(WebInfo.userAgent)
					.get();

			// TEST : Document doc = Jsoup.parse(new File("onnadaTest2.html"), "utf-8");
			Elements unitUrls = doc
					.select("body > div.layout-wrap > div > div > div.layout-line > div > ul > li > p.thumb > a");

			for (Element unitUrl : unitUrls) {
				OnnadaParser(unitUrl.absUrl("href"));
			}
			logger.info("Onnada.GetData(LocalDateTime date) is Executed END... : ");
		} catch (IOException e) {
			logger.warn(e);
			e.printStackTrace();
		}
	}

	private void OnnadaParser(String url) {
		try {
			logger.info("Onnada.OnnadaParser(String url) is Executed... : ");
			AniInfo aniInfo = new AniInfo();

			Document doc = Jsoup.connect(url).timeout(WebInfo.timeOut).userAgent(WebInfo.userAgent).get();
			// TEST : Document doc = Jsoup.parse(new File("onnadaTest1.html"), "utf-8");
			aniInfo.SetSourseURL(url.replace("wait", "hit"));
			logger.debug("SourseURL : " + aniInfo.GetSourseURL());

			aniInfo.SetKoreaTitle(
					doc.select("body > div.layout-wrap > div > div > article > div.view-title > h1").text());
			logger.debug("KoreaTitle : " + aniInfo.GetKoreaTitle());

			aniInfo.SetOriginalTitle(doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(원제) ~ span")
					.text());
			logger.debug("OriginalTitle : " + aniInfo.GetOriginalTitle());

			aniInfo.SetOriginal(doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(원작) ~ span")
					.text());
			logger.debug("Original : " + aniInfo.GetOriginal());

			aniInfo.SetDirector(doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(감독) ~ span")
					.text());
			logger.debug("Director : " + aniInfo.GetDirector());

			aniInfo.SetScenario(doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(각본) ~ span")
					.text());
			logger.debug("Scenario : " + aniInfo.GetScenario());

			aniInfo.SetCharacterDesign(doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(캐릭터 디자인) ~ span")
					.text());
			logger.debug("CharacterDesign : " + aniInfo.GetCharacterDesign());

			aniInfo.SetMusic(doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(음악) ~ span")
					.text());
			logger.debug("Music : " + aniInfo.GetMusic());

			aniInfo.SetCompany(doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(제작사) ~ span")
					.text());
			logger.debug("Company : " + aniInfo.GetCompany());

			aniInfo.SetGenre(doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(장르) ~ span")
					.text());
			logger.debug("Genre : " + aniInfo.GetGenre());

			String type = doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(분류) ~ span")
					.text().toLowerCase();
			if (type.contains("tv") || type.contains("web")) {
				aniInfo.SetType(EType.TV);
			} else if (type.contains("ova")) {
				aniInfo.SetType(EType.OVA);
			} else if (type.contains("movie")) {
				aniInfo.SetType(EType.Movie);
			}
			logger.debug("Type : " + aniInfo.GetType());

			Elements subTags = doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(키워드) ~ span");
			if (subTags.size() > 0) {
				final String patternString = "[^\\uAC00-\\uD7A3xfe0-9a-zA-Z\\s]|[;:\\?]";
				Pattern pattern = Pattern.compile(patternString);
				for (Element subTag : subTags) {
					String tag = subTag.text();
					aniInfo.AddSubTag(tag);
					if (pattern.matcher(tag).find()) {
						tag = tag.replaceAll(patternString, " ").replaceAll("(\\s){2,}", " ")
								.replaceAll("(^\\s*)|([\\s]*$)", "");
					}
				}
			} else {
				aniInfo.AddSubTag(aniInfo.GetEnglisTitle());
			}
			logger.debug("SubTag : " + aniInfo.GetSubTag());

			aniInfo.SetCountry(doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(제작국가) ~ span")
					.text());
			logger.debug("SubTag : " + aniInfo.GetCountry());

			String broadcastingDate = doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(방영일) ~ span")
					.text();
			logger.debug("broadcastingDate : " + broadcastingDate);
			if (broadcastingDate.isEmpty()) {
				throw new Exception();
			}

			if (broadcastingDate.contains("xx")) {
				broadcastingDate = broadcastingDate.replace("xx", "01");
			}
			if (!broadcastingDate.contains(":")) {
				broadcastingDate = broadcastingDate + " 00:00";
			}
			aniInfo.SetBroadcastingDate(broadcastingDate.replaceAll("\\s\\(.\\)", ""), "yyyy.MM.dd HH:mm");

			aniInfo.SetRating(doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(등급) ~ span")
					.text());
			logger.debug("Rating : " + aniInfo.GetRating());

			Elements subSites = doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(공식홈페이지) ~ span")
					.select("a");
			for (Element subSite : subSites) {
				aniInfo.AddSubSite(subSite.text());
			}
			subSites = doc.select(
					"body > div.layout-wrap > div > div > article > div.view-info > div.list > p > span:contains(공식트위터) ~ span")
					.select("a");
			for (Element subSite : subSites) {
				aniInfo.AddSubSite(subSite.text());
			}
			logger.debug("subSites : " + aniInfo.GetSubSite());

			aniInfo.SetImage(doc
					.select("body > div.layout-wrap > div > div > article > div.view-info > div.image > div > a > img")
					.attr("src"));
			logger.debug("Image : " + aniInfo.GetImage());

			aniList.add(aniInfo);
			logger.info("Onnada.OnnadaParser(String url) is Executed END... : ");
		} catch (IOException e) {
			logger.warn(e);
			e.printStackTrace();
		} catch (Exception e) {
			logger.warn(e);
			e.printStackTrace();
		}
	}
}